package com.jdd.userprofile.repository;

import com.jdd.userprofile.model.UserEntity;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface UserEntityRepository extends ReactiveCrudRepository<UserEntity, Long> {
    Mono<UserEntity> findUserEntityByEmail(String email);

    Flux<UserEntity> findUserEntitiesByFirstNameContainsIgnoreCaseOrLastNameContainingIgnoreCaseOrEmailContainingIgnoreCase(String firstName,String lastName,String email);





    @Query("SELECT p FROM UserEntity p WHERE CONCAT(p.firstName, p.lastName, p.email) LIKE %?1%")
    Flux<UserEntity> searchUsersByFirstNameLastNameAndEmail(String searchKey);
}
