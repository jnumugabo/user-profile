package com.jdd.userprofile.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name="user_entity")
public class UserEntity {
    @Id
    private Long id;
    private String firstName;
    private String lastName;

    private Boolean isActive;

    private String email;

    private String password;

    public UserEntity(Long id, String s, String s1, String s2) {
    }
}
