package com.jdd.userprofile.config;

import com.jdd.userprofile.model.UserEntity;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Sinks;

@Configuration
public class SinkConfig {

    @Bean
    public Sinks.Many<UserEntity> sink(){
        return Sinks.many().replay().all();
    }


}
