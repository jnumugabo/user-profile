package com.jdd.userprofile.controller;

import com.jdd.userprofile.dto.*;
import com.jdd.userprofile.model.UserEntity;
import com.jdd.userprofile.security.JWTUtil;
import com.jdd.userprofile.security.PBKDF2Encoder;
import com.jdd.userprofile.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final PBKDF2Encoder passwordEncoder;
    private final JWTUtil jwtUtil;

    private final Sinks.Many<UserEntity> userEntityMany;

    private static final String emailRegex = "^(.+)@(.+)$";
    private static final String passwordRegex = "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=.*[@#$%^&+=])" + "(?=\\S+$).{8,20}$";

    @GetMapping(value ="/getAll", produces = MediaType. TEXT_EVENT_STREAM_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Flux<UserEntity> getAllUsers(){
        return userService.findUsers();
    }

    @GetMapping(value ="/user/{id}", produces = MediaType. TEXT_EVENT_STREAM_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Mono<UserEntity> getUserById(@PathVariable long id){
        return userService.findUserEntityById(id);
    }

    @PostMapping(value = "/register")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<ResponseEntity<CredentialResponseDto>> addUser(@RequestBody UserEntity user){
        Pattern emailPattern = Pattern.compile(emailRegex);
        Pattern passwordPattern = Pattern.compile(passwordRegex);
        Matcher matcher = emailPattern.matcher(user.getEmail());
        Matcher passMatcher = passwordPattern.matcher(user.getPassword());
        if(!matcher.matches()){
            return Mono.just(ResponseEntity.ok(new CredentialResponseDto("Email is invalid")));
        }
        if(!passMatcher.matches()){
            return Mono.just(ResponseEntity.ok(new CredentialResponseDto("Password is invalid")));
        }
        return userService.createUser(user)
                .map(userEntity -> ResponseEntity.ok(new CredentialResponseDto("User registered successfully")));
    }

    @PutMapping(value = "edit/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<ResponseEntity<UserDto>> updateUser(@PathVariable long id, @RequestBody UserDto user){

        return userService.updateUser(id, user)
                .map(use -> ResponseEntity.ok(new UserDto(use.getFirstName(), use.getLastName(), use.getEmail())));

    }


    @PostMapping("/login")
    public Mono<ResponseEntity<AuthResponse>> login(@RequestBody AuthRequest ar) {
        return userService.findUserEntityByEmail(ar.getUsername())
                .filter(userDetails -> passwordEncoder.encode(ar.getPassword()).equals(userDetails.getPassword()))
                .map(userDetails -> ResponseEntity.ok(new AuthResponse(jwtUtil.generateToken(userDetails))))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()));
    }
    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<Void> deleteUser(@PathVariable long id){
        return userService.deleteUser(id);
    }
    @PostMapping("/change/credential")
    public Mono<ResponseEntity<CredentialResponseDto>> changeCredentials(@RequestBody CredentialDto credentialDto){
        return userService.changeCredentials(credentialDto)
                .map(userEntity -> ResponseEntity.ok(new CredentialResponseDto("Credential changed successfully")))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.BAD_REQUEST).build()));
    }

    @GetMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public Flux<UserEntity> searchUser(@RequestParam String searchKey){
        return userService.search(searchKey);
    }
}
