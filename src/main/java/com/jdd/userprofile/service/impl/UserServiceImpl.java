package com.jdd.userprofile.service.impl;

import com.jdd.userprofile.dto.CredentialDto;
import com.jdd.userprofile.dto.UserDto;
import com.jdd.userprofile.model.UserEntity;
import com.jdd.userprofile.repository.UserEntityRepository;
import com.jdd.userprofile.security.PBKDF2Encoder;
import com.jdd.userprofile.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;


@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserEntityRepository userEntityRepository;
    private final PBKDF2Encoder passwordEncoder;
    private final Sinks.Many<UserEntity> sink;

    public Mono<UserEntity> findUserEntityByEmail(String email) {
        return userEntityRepository.findUserEntityByEmail(email).switchIfEmpty(Mono.error(new RuntimeException("User not found")));
    }

    @Override
    public Mono<UserEntity> findUserEntityById(long id) {
        return userEntityRepository.findById(id).switchIfEmpty(Mono.error(new RuntimeException("User not found")));
    }

    public Flux<UserEntity> findUsers() {
        return userEntityRepository.findAll();
    }

    public Mono<UserEntity> createUser(UserEntity user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userEntityRepository.save(user)
                .doOnNext(this.sink::tryEmitNext);
    }

    @Override
    public Mono<UserEntity> updateUser(long id, UserDto user) {
        return userEntityRepository.findById(id)
                .flatMap(olderUser ->{

                        if(user.getFirstName() != null){
                            olderUser.setFirstName(user.getFirstName());
                        }

                        if(user.getLastName() != null){
                            olderUser.setLastName(user.getLastName());
                        }

                        if(user.getEmail() != null){
                            olderUser.setEmail(user.getEmail());
                        }
                    return userEntityRepository.save(olderUser)
                            .doOnNext(this.sink::tryEmitNext);
                })
                .switchIfEmpty(Mono.error(new RuntimeException("User not found")));
    }

    public Mono<UserEntity> blockUser(UserEntity user){
        return userEntityRepository.findById(user.getId())
                .switchIfEmpty(Mono.error(new RuntimeException("User not found")))
                .map(olderUser -> {
                            olderUser.setIsActive(false);
                            return olderUser;
                        })
                .flatMap(userEntityRepository::save);
    }

    @Override
    public Mono<Void> deleteUser(long id) {
        return userEntityRepository.deleteById(id);
    }

    @Override
    public Mono<UserEntity> changeCredentials(CredentialDto credentialDto) {
        return userEntityRepository.findUserEntityByEmail(credentialDto.getEmail())
                .switchIfEmpty(Mono.error(new RuntimeException("User not found")))
                .map(foundUser -> {
                    foundUser.setPassword(passwordEncoder.encode(credentialDto.getPassword()));
                    return foundUser;
                })
                .flatMap(userEntityRepository::save).doOnNext(this.sink::tryEmitNext);
    }

    @Override
    public Flux<UserEntity> search(String searchKey) {
        return userEntityRepository.findUserEntitiesByFirstNameContainsIgnoreCaseOrLastNameContainingIgnoreCaseOrEmailContainingIgnoreCase(searchKey, searchKey, searchKey);
    }
}
