package com.jdd.userprofile.service;

import com.jdd.userprofile.dto.CredentialDto;
import com.jdd.userprofile.dto.UserDto;
import com.jdd.userprofile.model.UserEntity;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserService {
   Mono<UserEntity> findUserEntityByEmail(String email);
   Mono<UserEntity> findUserEntityById(long id);
   Flux<UserEntity> findUsers();
   Mono<UserEntity> createUser(UserEntity user);
   Mono<UserEntity> updateUser(long id, UserDto user);

   Mono<UserEntity> blockUser(UserEntity user);
   Mono<Void> deleteUser(long id);
   Mono<UserEntity> changeCredentials(CredentialDto credentialDto);

   Flux<UserEntity> search(String searchKey);




}
